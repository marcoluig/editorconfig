# .editorconfig
`.editorconfig` helps maintain consistent coding styles for multiple developers working on the same project across various editors and IDEs.<br><br>

## Install Dependecies
1. If your editor or IDE doesn't provide a native handling for `.editorconfig` files, install an EditorConfig extension/plugin.<br><br>

## Installation via Composer
If you have a project based on Composer, your can install this package by the following steps:

1. To access this repository via Composer, you have to add it to the repositories array in your root `composer.json`
```
"repositories": [
    {
        "type": "git",
        "url": "https://marcoluig@bitbucket.org/marcoluig/editorconfig.git"
    }
]
```
<br>

2. install this package as dev dependency via:
```
composer req marcoluig/editorconfig --dev
```
<br>

3. Add a symlink of the `.editorconfig` file from `vendor` to `root` directory in your root `composer.json`.<br>
__Note:__ please change the `vendor` path if it differs in your Composer configuration.
```
"scripts": {
    "post-autoload-dump": [
      "ln -sf vendor/marcoluig/editorconfig/.editorconfig .editorconfig"
    ]
  },
```
<br><br>
## Manual installation
Download and copy the `.editorconfig` to yout project root directory.
